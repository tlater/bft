//! bft - A brainfuck interpreter

use std::path::PathBuf;

use structopt::StructOpt;

use bft_interp::VirtualMachine;
use bft_types::Program;

/// A brainfuck interpreter
#[derive(StructOpt)]
struct Opt {
    /// The path to the program to execute
    program: PathBuf,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    let program = Program::from_path(&opt.program);
    let vm: VirtualMachine<u8> = VirtualMachine::default();

    vm.execute(&program?);
    Ok(())
}
