//! The bft brainfuck interpreter

use bft_types::Program;

/// A brainfuck virtual machine.
pub struct VirtualMachine<T> {
    tape: Vec<T>,
    head: usize,
    dynamic: bool,
}

impl<T> VirtualMachine<T> {
    /// Construct a new `VirtualMachine<T>`.
    ///
    /// This virtual machine is conceptually a turing machine - made
    /// up of a tape that contains cells with values, and a head which
    /// moves along this tape. The brainfuck language is designed to
    /// manipulate this tape with instructions; a set of these are
    /// defined in the bft_types crate.
    ///
    /// Its capacity defines the default length of the tape on which
    /// the program is executed.
    ///
    /// If `dynamic` is false, the capacity becomes static and the
    /// interpreter will not change the tape's length.
    pub fn new(capacity: usize, dynamic: bool) -> Self {
        Self {
            tape: Vec::with_capacity(if capacity == 0 { 30_000 } else { capacity }),
            head: 0,
            dynamic
        }
    }

    /// Execute the given brainfuck program on the virtual machine.
    pub fn execute(&self, program: &Program) {
        for instruction in program.instructions() {
            println!("{:?}", instruction);
        }
    }
}

impl<T> Default for VirtualMachine<T> {
    fn default() -> Self {
        Self {
            tape: Vec::with_capacity(30_000),
            head: 0,
            dynamic: false,
        }
    }
}
