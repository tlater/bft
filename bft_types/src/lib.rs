//! Types for the bft brainfuck interpreter

use std::fs;
use std::iter::Iterator;
use std::path::{Path, PathBuf};

#[derive(Debug)]
/// A brainfuck program
pub struct Program {
    path: PathBuf,
    instructions: Vec<Instruction>,
}

impl Program {
    /// Constructs a new `Program`.
    ///
    /// The provided `path` is only used for debugging purposes, and
    /// is not assumed to be a real path. The `code`, however, should
    /// be a string that represents brainfuck instructions.
    ///
    /// # Examples
    /// ```
    /// # use bft_types::{Instruction, Program, InstructionType};
    /// use std::path::Path;
    ///
    /// let code = "++.";
    /// let program = Program::new(Path::new("/home/tomjon/test.bf"), code);
    /// let instructions = program.instructions();
    ///
    /// assert_eq!(instructions[0].instruction(), &InstructionType::Increment);
    /// assert_eq!(instructions[1].instruction(), &InstructionType::Increment);
    /// assert_eq!(instructions[2].instruction(), &InstructionType::Print);
    /// ```
    pub fn new(path: &Path, code: &str) -> Self {
        Program {
            path: path.to_path_buf(),
            instructions: parse_instructions(code),
        }
    }

    /// Read a `Program` from a file.
    pub fn from_path(path: &Path) -> Result<Self, Box<dyn std::error::Error>> {
        Ok(Self::new(path, fs::read_to_string(path)?.as_str()))
    }

    /// Return an iterator over the instructions of this program.
    pub fn instructions(&self) -> &Vec<Instruction> {
        &self.instructions
    }
}

fn parse_instructions(code: &str) -> Vec<Instruction> {
    code.lines()
        .enumerate()
        .flat_map(|(row, line)| {
            line.chars()
                .enumerate()
                .filter_map(move |(col, c)| Instruction::from_char(c, row, col))
        })
        .collect()
}

/// A brainfuck instruction
#[derive(Debug, PartialEq)]
pub struct Instruction {
    instruction: InstructionType,
    column: usize,
    row: usize,
}

impl Instruction {
    fn from_char(c: char, row: usize, col: usize) -> Option<Self> {
        InstructionType::from_char(c).map(|instruction| Self {
            instruction,
            column: col + 1,
            row: row + 1,
        })
    }

    /// Get the type of instruction.
    pub fn instruction(&self) -> &InstructionType {
        &self.instruction
    }
}

/// The "type" of instruction
///
/// These should be implemented taking [the inofficial
/// spec](https://github.com/brain-lang/brainfuck/blob/master/brainfuck.md)
/// into account.
#[derive(Debug, PartialEq)]
pub enum InstructionType {
    /// Start a loop
    ///
    /// This "jump[s] to the matching [LoopEnd] instruction if the current
    /// value is zero".
    LoopStart,
    /// End a loop
    ///
    /// This "jump[s] to the matching [LoopStart] instruction if the
    /// current value is *not* zero".
    LoopEnd,
    /// Move the pointer right
    Forward,
    /// Move the pointer left
    Backward,
    /// Increment the current cell
    Increment,
    /// Decrement the current cell
    Decrement,
    /// Output the value of the current cell
    Print,
    /// *Replace* the value of the current cell with input
    Read,
}

impl InstructionType {
    fn from_char(c: char) -> Option<Self> {
        match c {
            '[' => Some(InstructionType::LoopStart),
            ']' => Some(InstructionType::LoopEnd),
            '>' => Some(InstructionType::Forward),
            '<' => Some(InstructionType::Backward),
            '+' => Some(InstructionType::Increment),
            '-' => Some(InstructionType::Decrement),
            '.' => Some(InstructionType::Print),
            ',' => Some(InstructionType::Read),
            _ => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_instructions() {
        let code = ".,\n\
                    []\n\
                    <>\n\
                    +-";
        let program = Program::new(Path::new("/home/tomjon/test.bf"), code);
        let instructions = program.instructions();

        assert_eq!(
            instructions,
            &vec!(
                Instruction {
                    instruction: InstructionType::Print,
                    column: 1,
                    row: 1
                },
                Instruction {
                    instruction: InstructionType::Read,
                    column: 2,
                    row: 1
                },
                Instruction {
                    instruction: InstructionType::LoopStart,
                    column: 1,
                    row: 2
                },
                Instruction {
                    instruction: InstructionType::LoopEnd,
                    column: 2,
                    row: 2
                },
                Instruction {
                    instruction: InstructionType::Backward,
                    column: 1,
                    row: 3
                },
                Instruction {
                    instruction: InstructionType::Forward,
                    column: 2,
                    row: 3
                },
                Instruction {
                    instruction: InstructionType::Increment,
                    column: 1,
                    row: 4
                },
                Instruction {
                    instruction: InstructionType::Decrement,
                    column: 2,
                    row: 4
                },
            )
        )
    }
}
